﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnManager : MonoBehaviour {
    [SerializeField]
    private int numEnemies = 5;
    [SerializeField]
    private GameObject boss = null;
    [SerializeField]
    private GameObject[] enemiesToSpawn = null;
    //5 would mean spawn an enemy every 5 seconds.
    [SerializeField]
    private float spawnRate = 2;

    private GameObject enemyHolder;

    private float dt = 0;
    private int enemiesAlreadySpawned = 0;
    private int curEnemyToSpawn = 0;
    private float screenSize;
    private GameObject[] enemies;
    private GameObject bossObject;
    private bool spawnedBoss = false;
    private ScrollingBackgroundScript sbs;
    private AudioSource bossMusic;
    private AudioSource roundMusic;
    private GameObject scrBackground;
    private GameObject bossScrBackground;
    // Use this for initialization
    void Start ()
    {
        scrBackground = GameObject.Find("ScrollingBackground");
        bossScrBackground = GameObject.Find("BossScrollingBackground");
        if (bossScrBackground != null)
        {
            scrBackground.SetActive(true);
            bossScrBackground.SetActive(false);
        }
        roundMusic = GameObject.Find("Main Camera").GetComponent<AudioSource>();
        bossMusic = GetComponent<AudioSource>();
        sbs = GameObject.Find("ScrollingBackground").GetComponent<ScrollingBackgroundScript>();
        enemies = new GameObject[numEnemies];
        screenSize = Camera.main.orthographicSize;
        enemyHolder = GameObject.Find("Enemies");
        if(enemyHolder == null)
        {
            enemyHolder = GameObject.Find("EnemyPool");
        }
    }

    internal bool BossDefeated()
    {
        if(bossObject == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    internal void SpawnBoss()
    {
        if(!spawnedBoss)
        {
            roundMusic.Pause();
            bossMusic.Play();
            if(bossScrBackground != null)
            {
                scrBackground.SetActive(false);
                bossScrBackground.SetActive(true);
                //bossScrBackground.GetComponent<ScrollingBackgroundScript>().Scroll = false;
            }
            bossObject = (GameObject)Instantiate(boss, new Vector3(Camera.main.rect.xMax * (screenSize * 16 / 9) + 1, Camera.main.rect.yMax * screenSize - 1, 0), new Quaternion());
            EnemyMovement enemyMovement = bossObject.GetComponent<EnemyMovement>();
            enemyMovement.setMovementPattern((int)EnemyMovement.MovementPatterns.TRIANGLE);
            enemyMovement.setComeFromRight(true);
            spawnedBoss = true;
            sbs.Scroll = false;
        }
    }

    // Update is called once per frame
    void Update () {
        if(enemiesAlreadySpawned >= numEnemies)
        {
            
            return;
        }
        dt += Time.deltaTime;
        if(dt >= spawnRate)
        {
            GameObject enemy;
            curEnemyToSpawn = UnityEngine.Random.Range(0, enemiesToSpawn.Length);
            int movementPattern = UnityEngine.Random.Range(0, (int)EnemyMovement.MovementPatterns.CIRCLE + 1);
            if(enemiesAlreadySpawned %2 == 0)
            {
                enemy = (GameObject)Instantiate(enemiesToSpawn[curEnemyToSpawn], new Vector3(Camera.main.rect.xMax * (screenSize * 16 / 9) + 1, Camera.main.rect.yMax * screenSize - 1, 0), new Quaternion());
                enemies[enemiesAlreadySpawned] = enemy;
                EnemyMovement enemyMovement = enemy.GetComponent<EnemyMovement>();
                enemyMovement.setMovementPattern(movementPattern);
                enemyMovement.setComeFromRight(true);
            }
            else
            {
                enemy = (GameObject)Instantiate(enemiesToSpawn[curEnemyToSpawn], new Vector3(-Camera.main.rect.xMax * (screenSize * 16 / 9) - 1, Camera.main.rect.yMax * screenSize - 1, 0), new Quaternion());
                enemies[enemiesAlreadySpawned] = enemy;
                EnemyMovement enemyMovement = enemy.GetComponent<EnemyMovement>();
                enemyMovement.setMovementPattern(movementPattern);
                enemyMovement.setComeFromRight(false);
            }
            enemy.transform.SetParent(enemyHolder.transform);
            enemiesAlreadySpawned++;
            dt = 0;
        }
	}
    public GameObject[] Enemies()
    {
        return enemies;
    }

    public int EnemiesAlreadySpawned()
    {
        return enemiesAlreadySpawned;
    }
    
    public int NumEnemies()
    {
        return numEnemies;
    }
}
