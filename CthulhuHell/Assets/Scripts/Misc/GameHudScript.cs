﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameHudScript : MonoBehaviour
{
	[SerializeField]
	private Canvas gameHud;

	[SerializeField]
	private GameObject player;

	private Text scoreText;
	private const string SCORE = "Score: ";
	private Text goldText;
	private const string GOLD = "Gold: ";
    //private Text grazeText = null;
    //private const string GRAZE = "Graze: ";
	void Awake()
	{
		Time.timeScale = 1.0f;
	}

	void Start()
	{
		if (!player) player = GameObject.FindGameObjectWithTag ("Player");
		if (!gameHud) gameHud = GameObject.Find ("GameHud").GetComponent<Canvas> ();
        
		Transform hudBackground = gameHud.transform.Find ("HudBackground");

		scoreText = hudBackground.Find("Score").GetComponent<Text> ();
		scoreText.text = SCORE + PlayerInformation.playerInformation.playerData.Score;

		goldText = hudBackground.Find ("Gold").GetComponent<Text> ();
		goldText.text = GOLD + PlayerInformation.playerInformation.playerData.Gold;

		//grazeText = hudBackground.Find ("Graze").GetComponent<Text> ();
		//grazeText.text = GRAZE + PlayerInformation.playerInformation.playerData.Graze;
	}

	void Update()
	{
		//if (Time.timeScale != 0)
		//{
		//	scoreText.text = SCORE + PlayerInformation.playerInformation.playerData.Score;
		//	goldText.text = GOLD + PlayerInformation.playerInformation.playerData.Gold;
		//}
		//grazeText.text = GRAZE + PlayerInformation.playerInformation.playerData.Graze;
	}

    public void UpdateScore()
    {
        scoreText.text = SCORE + PlayerInformation.playerInformation.playerData.Score;
    }
    public void UpdateGold()
    {
        goldText.text = GOLD + PlayerInformation.playerInformation.playerData.Gold;
    }
}
