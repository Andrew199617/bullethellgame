﻿using UnityEngine;
using System.Collections;

public class PauseMenuScript : MonoBehaviour
{
	[SerializeField]
	private Canvas pauseMenu = null;

	public static bool IsPaused { get; set; }

	void Awake()
	{
		IsPaused = false;
	}

	void Update ()
	{
		if (Input.GetButtonDown ("Pause"))
		{
			IsPaused = !IsPaused;

			if (IsPaused)
			{
				Time.timeScale = 0.0f;
				ShowPauseMenu ();
			}
			else
			{
				Time.timeScale = 1.0f;
				HidePauseMenu ();
			}
		}
	}

	private void ShowPauseMenu ()
	{
		pauseMenu.gameObject.SetActive (true);
	}

	private void HidePauseMenu ()
	{
		pauseMenu.gameObject.SetActive (false);
	}

}
