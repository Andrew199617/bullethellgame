﻿using UnityEngine;
using System.Collections;

public class TutorialPickup : MonoBehaviour {

    public int Value { get; set; }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }

    public void Destroy()
    {
        gameObject.SetActive(false);
    }
}
