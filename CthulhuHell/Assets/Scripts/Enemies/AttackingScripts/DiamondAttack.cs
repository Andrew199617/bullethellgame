﻿using UnityEngine;
using System.Collections;

public class DiamondAttack : EnemyAttack {

    private bool allBulletsAreNull = true;

    private int bulletsInSpreadOffset = -1;
    private int bulletOffset = 0;
    private int bulletsInSpread = 5; 
    private Renderer rend;
    private int longestRow = 5;
    private float dt = 0;
    [SerializeField]
    private float bulletsPerSecond = 1;
    [SerializeField]
    private GameObject bullet = null;

    private GameObject bulletHolder;

    private BulletScript[] bulletScripts;

    void Start()
    {
        bulletScripts = new BulletScript[numBullets];
        bullets = new GameObject[numBullets];
        rend = this.GetComponent<Renderer>();
        bulletHolder = GameObject.Find("BulletPool");
    }

    void Update()
    {
        if (rend.isVisible)
        {
            if (dt >= 1 / bulletsPerSecond)
            {
                allBulletsAreNull = true;
                for (int i = bulletOffset; i < bulletsInSpread + bulletOffset; i++)
                {
                    if (bullets[i] != null && bullets[i].activeSelf)
                    {
                        allBulletsAreNull = false;
                    }
                    else if (bullets[i] == null)
                    {
                        bullets[i] = (GameObject)(Instantiate(bullet, this.transform.position, new Quaternion()));
                        bullets[i].transform.SetParent(bulletHolder.transform);

                        bulletScripts[i] = bullets[i].GetComponent<BulletScript>();
                        bulletScripts[i].MovementSpeed = bulletSpeed;
                    }
                }
                if (allBulletsAreNull)
                {
                    for (int i = bulletOffset; i < bulletsInSpread + bulletOffset; i++)
                    {
                        bullets[i].transform.position = this.gameObject.transform.position;
                        bullets[i].SetActive(true);
                        bulletScripts[i].MovementSpeed = bulletSpeed;
						bulletScripts[i].bulletParticle = null;
                        bulletScripts[i].resetDt();
                        Vector3 movDirection = new Vector3();
                        if (this.transform.position.x > 0)
                        {
                            movDirection = Vector3.down + Vector3.left;
                        }
                        else
                        {
                            movDirection = Vector3.down + Vector3.right;
                        }
                        bulletScripts[i].MovementDirection = movDirection.normalized;
                        int multiple = i - bulletOffset;
                        int middle = bulletsInSpread/2;
                        if (bulletsInSpread % 2 == 1)
                        {
                            if (multiple == bulletsInSpread / 2 - .5)
                            {
                                bulletScripts[i].shortMovement = Vector3.zero;
                            }
                            else if (multiple < bulletsInSpread / 2)
                            {
                                bulletScripts[i].shortMovement = Vector3.left * ((i - bulletOffset) + 1);
                            }
                            else
                            {
                                bulletScripts[i].shortMovement = Vector3.right * (i - bulletOffset - middle);
                            }
                        }
                        else
                        {
                            if (multiple < bulletsInSpread / 2)
                            {
                                bulletScripts[i].shortMovement = Vector3.left * ((i - bulletOffset) + .5f);
                            }
                            else
                            {
                                bulletScripts[i].shortMovement = Vector3.right * ((i - bulletOffset - bulletsInSpread / 2) + .5f);
                            }
                        }
                        bulletScripts[i].shortMovement /= 2;
                    }
                    bulletOffset += bulletsInSpread;
                    bulletsInSpread += bulletsInSpreadOffset;
                    if(bulletsInSpread <= 1)
                    {
                        bulletsInSpreadOffset = 1;
                    }
                    if(bulletsInSpread >= longestRow)
                    {
                        bulletsInSpreadOffset = -1;
                    }
                    if(bulletOffset >= numBullets- longestRow)
                    {
                        bulletOffset = 0;
                        bulletsInSpread = 4;
                        bulletsInSpreadOffset = -1;
                    }

                }

                dt = 0;
            }
            else
            {
                dt += Time.deltaTime;
            }
        }
    }
}
