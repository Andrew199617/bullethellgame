﻿using UnityEngine;
using System.Collections;

public class WebAttack : EnemyAttack {
    [SerializeField]
    private GameObject ghost = null;
    [SerializeField]
    [Range(0, 1)]
    private int typeOfAttack = 0;
    private GameObject player;
    private GameObject bulletHolder;
    // Use this for initialization
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        bulletHolder = GameObject.Find("BulletPool");
        bullets = new GameObject[numBullets];
	    for( int i = 0; i < numBullets;i++)
        {
            bullets[i] = Instantiate(ghost);
            bullets[i].transform.SetParent(bulletHolder.transform);
            bullets[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < numBullets; i++)
        {
            if (bullets[i] == null || (bullets[i] != null && !bullets[i].activeSelf))
            {
                bullets[i].transform.position = new Vector3(( -5 + i *2),4,0);
                bullets[i].transform.localScale = Vector3.one * bulletSize;
                bullets[i].tag = "EnemyBullet";
                bullets[i].layer = 9;

                BulletScript bulletScript = bullets[i].GetComponent<BulletScript>();
                if(typeOfAttack == 0)
                {
                    bulletScript.MovementDirection = bullets[i].transform.position + Vector3.down * 10;
                    bulletScript.shortMovement = -bulletScript.MovementDirection;
                }
                else if(typeOfAttack == 1)
                {
                    bulletScript.MovementDirection = (player.transform.position - bullets[i].transform.position).normalized;
                }
                bulletScript.MovementSpeed = bulletSpeed;

                bullets[i].SetActive(true);
            }
        }

    }
}
