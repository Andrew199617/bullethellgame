﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
	private int currentPrompt = 0;
	private Text text;
	[SerializeField]
	private Button no = null;
	[SerializeField]
	private Button yes = null;
	[SerializeField]
	private GameObject goldPickup = null;
	[SerializeField]
	private GameObject enemy = null;

	private GameObject goldObj;
	private GameObject enemyObj;

	void Start ()
	{
		text = this.GetComponentInChildren<Text> ();
		Time.timeScale = 1.0f;
        PauseMenuScript.IsPaused = false;
        Yes();
    }

	void Update ()
	{
		if (currentPrompt == 1 || currentPrompt == 2)
		{
			if (!goldObj.activeInHierarchy)
			{
				Yes ();
			}
		}
		else if (currentPrompt == 3)
		{
			if (!enemyObj.activeSelf)
			{
				Yes ();
			}
		}
	}

	public void Yes ()
	{
		switch (currentPrompt)
		{
		case 0:
			currentPrompt++;
			yes.gameObject.SetActive (false);
			no.gameObject.SetActive (false);
			goldObj = (GameObject)Instantiate (goldPickup, new Vector3 (4, 0), new Quaternion ());
			goldObj.GetComponent<PickupScript> ().Type = PickupScript.PickupType.GOLD;
			goldObj.GetComponent<PickupScript> ().OnEnable ();
			goldObj.GetComponent<PickupScript> ().fallSpeed = 0.0f;
			text.text = "Use WASD or the Arrow Keys to move. Try to pick up the gold.";
			break;
		case 1:
			currentPrompt++;
			goldObj = (GameObject)Instantiate (goldPickup, new Vector3 (-4, 0), new Quaternion ());
			goldObj.GetComponent<PickupScript> ().Type = PickupScript.PickupType.SCORE;
			goldObj.GetComponent<PickupScript> ().OnEnable ();
			goldObj.GetComponent<PickupScript> ().fallSpeed = 0.0f;
			text.text = "Now try picking up the points.";
			break;
		case 2:
			currentPrompt++;
			enemyObj = (GameObject)Instantiate (enemy, new Vector3 (0, 4), new Quaternion ());
			text.text = "Shoot at the enemy using Z, J, or Space.";
			break;
		case 3:
			currentPrompt++;
			yes.GetComponentInChildren<Text> ().text = "Continue";
			yes.transform.localPosition = new Vector3 (-75, -189, 0);
			yes.gameObject.SetActive (true);
			text.text = "You now have enough knowledge to play. Have Fun!";
			break;
		case 4:
			SceneManager.LoadScene (0);
			break;
		}
	}

	public void No ()
	{
		switch (currentPrompt)
		{
		case 0:
			SceneManager.LoadScene (0);
			break;

		}
	}
}
