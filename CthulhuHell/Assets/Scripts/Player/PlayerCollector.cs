﻿using UnityEngine;
using System.Collections;

public class PlayerCollector : MonoBehaviour
{
	private float grazeWait;

	private PlayerStats playerStats;
    [SerializeField]
    private GameObject killPlayer = null;
    private PlayerKiller playerKiller;
    private GameHudScript gameHudScript;
	void Start ()
	{
        gameHudScript = GameObject.Find("GameManager").GetComponent<GameHudScript>();
		playerStats = GetComponentInParent<PlayerStats> ();
        playerKiller = killPlayer.GetComponent<PlayerKiller>();
        
	}

	void Update ()
	{
		if (grazeWait > 0)
		{
			grazeWait -= 10.0f;
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "EnemyBullet" && grazeWait <= 0)
		{
			PlayerInformation.playerInformation.playerData.Graze++;
			grazeWait = 60.0f;
		}
		else
		{
			switch (other.tag)
			{
			case "Pickup_SCORE":
                PlayerInformation.playerInformation.playerData.Score += other.GetComponent<PickupScript> ().Value;
				other.gameObject.SetActive (false);
                gameHudScript.UpdateScore();
				break;
			case "Pickup_GOLD":
				PlayerInformation.playerInformation.playerData.Gold += other.GetComponent<PickupScript> ().Value;
				other.gameObject.SetActive (false);
                gameHudScript.UpdateGold();
				break;
			case "Pickup_LIFE":
				if (PlayerInformation.playerInformation.playerData.Lives < playerStats.MAX_LIVES)
				{
                    PlayerInformation.playerInformation.playerData.Lives++;
					other.gameObject.SetActive (false);
				}
                playerKiller.updateHearts();
                break;
			}
		}
	}

}
