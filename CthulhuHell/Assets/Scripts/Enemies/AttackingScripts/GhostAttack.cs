﻿using UnityEngine;
using System.Collections;

public class GhostAttack : EnemyAttack {
    [SerializeField]
    private GameObject ghost = null;
    [SerializeField]
    [Range(0, 1)]
    private int typeOfAttack = 0;
    private GameObject player;
    private GameObject bulletHolder;
    private BulletScript[] bulletScripts;
    // Use this for initialization
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        bulletHolder = GameObject.Find("BulletPool");
        bullets = new GameObject[numBullets];
        bulletScripts = new BulletScript[numBullets];
	    for( int i = 0; i < numBullets;i++)
        {
            bullets[i] = Instantiate(ghost);
            bullets[i].transform.SetParent(bulletHolder.transform);
            bulletScripts[i] = bullets[i].GetComponent<BulletScript>();
            bullets[i].SetActive(false);
        }
	}

    public void OnEnable()
    {
        if (bulletScripts == null)
        {
            return;
        }
        for (int i = 0;i < numBullets;i++)
        {
            bulletScripts[i].resetDt();
        }
    }

    // Update is called once per frame
    void Update ()
    {
        for (int i = 0; i < numBullets; i++)
        {
            if (bullets[i] == null || (bullets[i] != null && !bullets[i].activeSelf))
            {
                bullets[i].transform.position = new Vector3(( -5 + i *2),4,0);
                bullets[i].transform.localScale = Vector3.one * bulletSize;
                bullets[i].tag = "EnemyBullet";
                bullets[i].layer = 9;
                
                if(typeOfAttack == 0)
                {
                    bulletScripts[i].MovementDirection = bullets[i].transform.position + Vector3.down * 10;
                    bulletScripts[i].shortMovement = -bulletScripts[i].MovementDirection;
                    bulletScripts[i].lengthOfShortMovement = .5f;
                }
                else if(typeOfAttack == 1)
                {
                    bulletScripts[i].MovementDirection = (player.transform.position - bullets[i].transform.position).normalized;
                }
                bulletScripts[i].MovementSpeed = bulletSpeed;

                bullets[i].SetActive(true);
            }
        }

    }
}
