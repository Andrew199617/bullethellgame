﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyMovement : MonoBehaviour
{
    public enum MovementPatterns
    {
        LEFTTORIGHT,
        TRIANGLE,
        CIRCLE
    }

	private float speed;
    private float size;
    private bool onScreen = false;
    private int movementPattern = (int)MovementPatterns.LEFTTORIGHT;
    private bool comeFromRight;

    private Vector3 rotationPoint;
    private int numRotations = 3;
    private float radius = 1.0f;

    public Vector3[] traingleWaypoints;
    private int currentWaypoint;

    private bool moveLeft = true;
    private Rigidbody2D rigidBody;
	void Start ()
	{
        rigidBody = this.GetComponent<Rigidbody2D>();
        rotationPoint = new Vector3(transform.position.x, 3.5f, 0);
        size = Camera.main.orthographicSize;
        //this.transform.position = new Vector3(Camera.main.rect.Max * (size * 16/9) + 5, Camera.main.rect.yMax * size, 0);
		speed = this.GetComponent<EnemyStats>().Speed;
	}
	
	void Update ()
	{
        if(!onScreen)
            GetOnScreen();	
        else
        {
            switch (movementPattern)
            {
                case (int)MovementPatterns.LEFTTORIGHT:
                    MoveLeftToRight();
                    break;
                case (int)MovementPatterns.TRIANGLE:
                    MoveInTraiangle();
                    break;
                case (int)MovementPatterns.CIRCLE:
                    MoveInCircle();
                    break;
            }
            
        }
	}

    private void MoveInCircle()
    {
        if (moveLeft)
        {
            rotationPoint += new Vector3(-1, 0, 0) * speed/numRotations * Time.deltaTime;
            if (rotationPoint.x < Camera.main.rect.xMin * (size * 16 / 9) - 4)
            {
                moveLeft = false;
            }
        }
        else
        {
            rotationPoint += new Vector3(1, 0, 0) * speed / numRotations * Time.deltaTime;
            if (rotationPoint.x > Camera.main.rect.xMax * (size * 16 / 9) - 1)
            {
                moveLeft = true;
            }
        }

        Vector3 position = transform.position;
        Vector3 normal = rotationPoint - position;

        Vector3 normalLength = normal;
        normalLength.Normalize();
        Vector3 desiredLength = normalLength * radius;
        desiredLength = normal - desiredLength;

        Vector3 linearVelocity = new Vector3(-normal.y, normal.x, 0);
        linearVelocity.Normalize();

        Vector3 velocity = (linearVelocity) + (desiredLength);
        rigidBody.transform.position += velocity * speed * Time.deltaTime ;
    }

    protected void GetOnScreen()
    {
        if(comeFromRight)
            rigidBody.transform.position += new Vector3(-1, 0, 0) * speed * Time.deltaTime;
        else if(!comeFromRight)
            rigidBody.transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;
    }

    protected void MoveInTraiangle()
    {
        if(traingleWaypoints.Length == 0)
        {
            MoveLeftToRight();
            return;
        }
        Vector3 direction = traingleWaypoints[currentWaypoint] - this.transform.position;
        if(direction.magnitude < .1f)
        {
            currentWaypoint++;
            if (currentWaypoint == traingleWaypoints.Length)
            {
                currentWaypoint = 0;
            }
        }
        direction.Normalize();
        rigidBody.transform.position += direction * speed * Time.deltaTime;
    }

    protected void MoveLeftToRight()
    {
        if (moveLeft)
        {
            rigidBody.transform.position += new Vector3(-1, 0, 0) * speed * Time.deltaTime;
            if (rigidBody.transform.position.x < Camera.main.rect.xMin * (size * 16 / 9) - 4)
            {
                moveLeft = false;
            }
        }
        else
        {
            rigidBody.transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;
            if (rigidBody.transform.position.x > Camera.main.rect.xMax * (size * 16 / 9) - 1)
            {
                moveLeft = true;
            }
        }
    }

    void OnBecameInvisible()
    {
        onScreen = false;
        //Destroy(this.gameObject);
    }

    void OnBecameVisible()
    {
        onScreen = true;
    }

    public bool OnScreen()
    {
        return onScreen;
    }

    public void setMovementPattern(int mp)
    {
        movementPattern = mp;
    }

    public void setComeFromRight(bool right)
    {
        this.comeFromRight = right;
    }
}
