﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(EnemyAttack))]
public class AttackManager : MonoBehaviour {
    public float TimeToChangeAttack = 5;
    private EnemyAttack[] attacks;
    private int currentAttack = 0;
    private float dt = 0;
	// Use this for initialization
	void Start () {
        attacks = this.GetComponents<EnemyAttack>();
        for(int i =0;i > attacks.Length;i++)
        {
            attacks[i].enabled = false;
        }
        currentAttack = Random.Range(0, attacks.Length);
        attacks[currentAttack].enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        dt += Time.deltaTime;
        if(dt > TimeToChangeAttack)
        {
            attacks[currentAttack].enabled = false;
            currentAttack = Random.Range(0, attacks.Length);
            
            attacks[currentAttack].enabled = true;
            dt = 0;
        }
	}
}
