﻿using UnityEngine;
using System.Collections;
using System;

public class ExplosionScript : MonoBehaviour {

    [SerializeField]
    private float minLifeTime = 1;
    [SerializeField]
    private float maxLifeTime = 3;
    [SerializeField]
    private ParticleSystem explosion = null;
    [SerializeField]
    private Sprite explodingSprite = null;
    private SpriteRenderer spriteRederer;

    private AudioSource explosionAudio;
    private float dt = 0;
    private float currentLifeTime = 0;
    

	// Use this for initialization
	void Start () {
        if (maxLifeTime < 1)
            maxLifeTime = 1;
        currentLifeTime = UnityEngine.Random.Range(minLifeTime, maxLifeTime);
        spriteRederer = GetComponent<SpriteRenderer>();
        explosionAudio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        dt += Time.deltaTime;
        if(dt >= currentLifeTime - 2)
        {
            spriteRederer.sprite = explodingSprite;
        }
        if(dt >= currentLifeTime)
        {
            Explode();
        }
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag.CompareTo("PlayerBullet") == 0)
        {
            other.gameObject.SetActive(false);
            Explode();
        }
    }
    private void Explode()
    {
        Instantiate(explosion, this.transform.position, new Quaternion());
        explosionAudio.Play();
        this.gameObject.SetActive(false);
    }

    public float LifeTime()
    {
        return maxLifeTime;
    }
}
