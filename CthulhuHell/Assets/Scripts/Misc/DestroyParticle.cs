﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyParticle : MonoBehaviour {

    private AudioSource explosionAudio;
	// Use this for initialization
	void Start () {
        explosionAudio = GetComponent<AudioSource>();
        if (explosionAudio != null)
            explosionAudio.Play();
        Destroy(this.gameObject, this.GetComponent<ParticleSystem>().duration);
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
