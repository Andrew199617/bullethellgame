﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class BulletScript : MonoBehaviour
{
	private Rigidbody2D body2D;
	private Renderer rend;

    private float dt = 0;
    public float lengthOfShortMovement = 2;
    public Vector3 shortMovement = new Vector3();

	public float Damage { get; set; }
	public Vector3 MovementDirection { get; set; }
	public float MovementSpeed { get; set; }
	public float LookAt { get; set; }
	public ParticleSystem bulletParticle { get; set; }
	private ParticleSystem currentParticle;
	private Vector3 thePos;

	void Start ()
	{
		body2D = this.GetComponent<Rigidbody2D> ();
		rend = this.GetComponent<Renderer> ();
	}

    public void OnEnable()
    {
        transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);
        transform.rotation = Quaternion.AngleAxis(LookAt, Vector3.forward);
        if (bulletParticle != null)
        {
            thePos = this.transform.position;
            thePos.z = -1.0f;
            currentParticle = (ParticleSystem)Instantiate(bulletParticle, thePos, Quaternion.identity);
            currentParticle.transform.SetParent(this.transform);
            currentParticle.Play();
        }
    }

	public void OnDisable()
	{
		if (currentParticle != null)
		{
			Destroy (currentParticle.gameObject, 0.5f);
//			bulletParticle.Stop ();
		}
	}

	void Update ()
	{
        if(dt < lengthOfShortMovement)
        {
            body2D.velocity = (shortMovement + MovementDirection) * MovementSpeed;
            dt += Time.deltaTime;
        }
        else
        {
            body2D.velocity = MovementDirection * MovementSpeed;
        }

		if (bulletParticle)
			bulletParticle.transform.position = thePos;

		if (!rend.isVisible)
			Destory();
	}

	virtual public void Destory()
	{
		gameObject.SetActive (false);
	}

    public void resetDt()
    {
        dt = 0;
    }

}
