﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    [SerializeField]
	private Canvas gameOverScreen = null;

    public void GameOver ()
	{
		if (gameOverScreen) gameOverScreen.gameObject.SetActive (true);
	}

	public void Retry ()
	{
		Time.timeScale = 1.0f;
        PlayerInformation.playerInformation.Load();
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void BackToMenu ()
	{
		Time.timeScale = 1.0f;
		SceneManager.LoadScene ("Menu");
	}

}
