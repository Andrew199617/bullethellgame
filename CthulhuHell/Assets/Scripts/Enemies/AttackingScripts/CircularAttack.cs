﻿using UnityEngine;
using System.Collections;

public class CircularAttack : EnemyAttack
{
	private bool allBulletsAreNull = true;

	private Renderer rend;

	void Start ()
	{
		bullets = new GameObject[numBullets];
		rend = this.GetComponent<Renderer> ();
    }

	void Update ()
	{
		//TODO Try to clean more
		if (rend.isVisible)
        {
            allBulletsAreNull = true;
            for (int i = 0; i < numBullets; i++)
            {
				if (bullets[i] != null && bullets[i].activeSelf)
                {
					allBulletsAreNull = false;
					break;
                }
            }
            if (allBulletsAreNull)
            {
                for (int i = 0; i < numBullets; i++)
                {
                    float spawnDistance = 1;
					float halfSpawnDist = spawnDistance / 2;
					int halfNumBullets = numBullets / 2;

                    float index = i;

					if (index > halfNumBullets)
						index -= halfNumBullets;

					float percent = index / halfNumBullets;
                    percent *= spawnDistance;

					float xOffset = halfSpawnDist - percent;

					float yOffset = i > halfNumBullets ? 
						(percent <= halfSpawnDist ? -percent : -spawnDistance + percent) :
						(percent <= halfSpawnDist ? percent : spawnDistance - percent);

					bullets[i] = ObjectPoolerScript_Static.currentPooler.GetPooledObject ();
					bullets[i].transform.position = transform.position;
					bullets[i].GetComponent<SpriteRenderer> ().sprite = bulletSprite;
					//bullets[i].GetComponent<CircleCollider2D>().offset = new Vector2(0.0f, 0.0f);
					bullets[i].transform.localScale = Vector3.one * bulletSize;
					bullets[i].tag = "EnemyBullet";
                    bullets[i].layer = 9;

                    BulletScript bulletScript = bullets[i].GetComponent<BulletScript>();
					bulletScript.MovementSpeed = bulletSpeed;
					bulletScript.LookAt = 0.0f;
					bulletScript.MovementDirection = new Vector3(xOffset, yOffset).normalized;
					bulletScript.bulletParticle = null;
					bullets[i].SetActive (true);

                }

                allBulletsAreNull = false;
            }
        }
	}
}
