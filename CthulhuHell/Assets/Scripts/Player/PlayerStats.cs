﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour
{
	public readonly int MAX_LIVES = 5;

	public float PlayerInvincibiltyTime { get; set; }

    [SerializeField]
	[Range (1.0f, 3.0f)]
	public float invincibiltyTime = 1.0f;
    [SerializeField]
    private GameObject deathAnimation = null;
    

    private SpriteRenderer spriteRender;

	void Start ()
	{
        transform.position = new Vector3(0, -4, 0);

        invincibiltyTime *= 10;
	
        spriteRender = this.GetComponent<SpriteRenderer>();

    }

	void Update ()
	{
		if (PlayerInvincibiltyTime > 0)
		{
			PlayerInvincibiltyTime -= 10 * Time.deltaTime;
            Blink();
        }
	}

    public void GameOver ()
	{
		GameObject manager = GameObject.Find ("GameManager");
		manager.GetComponent<GameOverScript> ().GameOver();
        this.gameObject.SetActive(false);
        Time.timeScale = 0.0f;
	}

    private void Blink()
    {
        if(PlayerInvincibiltyTime % 2 < 1f)
        {
            spriteRender.color = new Color(255, 0, 0, 1f);
        }
        else
        {
            spriteRender.color = new Color(255, 255, 255, 1f);
        }
        if(PlayerInvincibiltyTime < 0)
        {
            spriteRender.color = new Color(255, 255, 255, 1f);
        }
    }

    public IEnumerator GotHit(CircleCollider2D collider)
    {
        collider.enabled = false;
        spriteRender.enabled = false;
        Instantiate(deathAnimation, this.transform.position, new Quaternion());

        yield return new WaitForSeconds(1f);

        collider.enabled = true;
        spriteRender.enabled = true;
        PlayerInvincibiltyTime = invincibiltyTime;
        transform.position = new Vector3(0,-4,0);
    }
}
