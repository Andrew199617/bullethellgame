﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    
    void Start()
    {
        PlayerInformation.playerInformation.Load();
        int curRound = PlayerInformation.playerInformation.playerData.CurrentRound;
        if(curRound <= 1)
        {
            GameObject.Find("GameBtn").SetActive(false);
        }
        else
        {
            GameObject.Find("NewGameBtn").transform.position += Vector3.up * 90;
        }
    }

    public void NewGame()
    {
        PlayerInformation.playerInformation.playerData.ReInitialize();
        PlayerInformation.playerInformation.Save();
        SceneManager.LoadScene("Round1");
    }

	public void LoadGame ()
	{
        int curRound = PlayerInformation.playerInformation.playerData.CurrentRound;
        SceneManager.LoadScene("Round" + curRound);
	}
    
	public void LoadUpgrades ()
	{
		SceneManager.LoadScene (2);
	}

    public void LoadGraze()
    {
        SceneManager.LoadScene(4);
    }

	public void LoadTutorial()
	{
		SceneManager.LoadScene (5);
	}

    public void LoadMenu ()
	{
		SceneManager.LoadScene (0);
	}

	public void BackToMenuSave()
	{
		PlayerInformation.playerInformation.Save ();
		SceneManager.LoadScene (0);
	}

    public void RoundOverLoadMenu()
    {
        string currentRound = SceneManager.GetActiveScene().name.Substring(5);
        int curRound = int.Parse(currentRound);
        PlayerInformation.playerInformation.playerData.CurrentRound = curRound + 1;
        PlayerInformation.playerInformation.Save();
        SceneManager.LoadScene(0);
    }
    public void LoadMenuAndSave()
    {
        PlayerInformation.playerInformation.playerData.AddHighScore(PlayerInformation.playerInformation.playerData.Score);
        PlayerInformation.playerInformation.playerData.ReInitialize();
        PlayerInformation.playerInformation.Save();
        SceneManager.LoadScene(0);
    }
    public void LoadHighScores()
    {
        SceneManager.LoadScene("HighScores");
    }
    public void SaveHighScore()
    {
        PlayerInformation.playerInformation.playerData.AddHighScore(PlayerInformation.playerInformation.playerData.Score);
        PlayerInformation.playerInformation.playerData.ReInitialize();
        PlayerInformation.playerInformation.Save();
        SceneManager.LoadScene("HighScores");
    }

    public void Exit ()
	{
		Application.Quit ();
	}

	public void LoadNextRound()
	{
		string currentRound = SceneManager.GetActiveScene().name.Substring(5);
		int curRound = int.Parse(currentRound);
        PlayerInformation.playerInformation.playerData.CurrentRound = curRound + 1;
        PlayerInformation.playerInformation.Save();
        SceneManager.LoadScene("Round" + (curRound + 1));
	}

}
