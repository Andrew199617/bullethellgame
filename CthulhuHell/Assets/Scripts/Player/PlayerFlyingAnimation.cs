﻿using UnityEngine;
using System.Collections;

public class PlayerFlyingAnimation : MonoBehaviour {
    [SerializeField]
    private ParticleSystem flyingAnimation = null;
    private Vector3 currentPosition;
    private ParticleSystem ps;
    public GameObject test;
    // Use this for initialization
    void Start () {
        currentPosition = this.gameObject.transform.position;
        ps = (ParticleSystem)Instantiate(flyingAnimation, this.gameObject.transform.position, flyingAnimation.transform.rotation);
        ps.Play();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        ps.transform.position = this.gameObject.transform.position;
        if (this.currentPosition.y < this.gameObject.transform.position.y &&
            this.currentPosition.x == this.gameObject.transform.position.x)
        {
            ps.startSize = .72f;
            ps.startLifetime = .72f;
            ps.startSpeed = 1.74f;
            ps.transform.LookAt(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0));
        }
        else if (this.currentPosition.y < this.gameObject.transform.position.y && ((this.currentPosition.x < 0 && currentPosition.x > gameObject.transform.position.x) ||
                (this.currentPosition.x > 0 && this.currentPosition.x < gameObject.transform.position.x)))
        {
            ps.startSize = .7f;
            ps.startLifetime = .7f;
            ps.startSpeed = 1.7f;
            ps.transform.LookAt((currentPosition - gameObject.transform.position).normalized - Vector3.up * 30);
        }
        else if (this.currentPosition.y < this.gameObject.transform.position.y && (this.currentPosition.x > 0 && this.currentPosition.x > gameObject.transform.position.x))
        {
            ps.startSize = .7f;
            ps.startLifetime = .7f;
            ps.startSpeed = 1.7f;
            Vector3 temp = (currentPosition - gameObject.transform.position).normalized;
            ps.transform.LookAt(temp + Vector3.right * 7.5f - Vector3.up * 30);
        }
        else if (this.currentPosition.y < this.gameObject.transform.position.y && (this.currentPosition.x < 0 && currentPosition.x < gameObject.transform.position.x))
        {
            ps.startSize = .7f;
            ps.startLifetime = .7f;
            ps.startSpeed = 1.7f;
            Vector3 temp = (currentPosition - gameObject.transform.position).normalized;
            ps.transform.LookAt(temp + Vector3.left * 7.5f - Vector3.up * 30);
        }
        else if (this.currentPosition.y > this.gameObject.transform.position.y)
        {
            ps.startSize = .5f;
            ps.startLifetime = .5f;
            ps.startSpeed = 1.0f;
            ps.transform.LookAt((this.gameObject.transform.position - Vector3.up));
        }
        else
        {
            ps.transform.LookAt(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0));
            ps.startSize = .6f;
            ps.startLifetime = .6f;
            ps.startSpeed = 1.35f;
        }
        currentPosition = this.gameObject.transform.position;
	    
	}
}
