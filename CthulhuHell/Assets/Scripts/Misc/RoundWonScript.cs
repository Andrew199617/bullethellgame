﻿using UnityEngine;
using System.Collections;

public class RoundWonScript : MonoBehaviour
{
	[SerializeField]
	private Canvas roundOverScreen = null;

	private SpawnManager spawnManager;
	private bool roundOver = true;
	private bool showingCanvas = false;

	void Start ()
	{
		spawnManager = this.GetComponent<SpawnManager> ();
	}

	void Update ()
	{
		if (showingCanvas || spawnManager.NumEnemies () > spawnManager.EnemiesAlreadySpawned ())
		{
			return;
		}
		roundOver = true;
		for (int i = 0; i < spawnManager.Enemies ().Length; i++)
		{
			if (spawnManager.Enemies ()[i].activeSelf)
			{
				roundOver = false;
			}
		}
		if (roundOver)
		{
			spawnManager.SpawnBoss ();
			if (spawnManager.BossDefeated ())
			{
				roundOverScreen.gameObject.SetActive (true);
				showingCanvas = true;
			}
		}
	}

}
