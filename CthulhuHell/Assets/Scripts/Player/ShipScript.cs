﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(Rigidbody2D))]
public class ShipScript : MonoBehaviour
{

	#region MovementVariables

	//TODO Change this
	[SerializeField]
	[Range (4.0f, 8.0f)]
	public float movementSpeed = 4.0f;

	private const float REDUCED_MOVEMENT_SPEED = 2.5f;

	private Rigidbody2D body2D;

	private float xMin, xMax, yMin, yMax;

	#endregion

	#region ShootingVariables

	[SerializeField]
	[Range (0.5f, 10.0f)]
	private float bulletSpeed = 1.0f;
	[SerializeField]
	public float bulletDamage = 1;

	[SerializeField]
	[Range (0.0f, 0.75f)]
	public float fireRate = 0.0f;

	private float cooldown = 0;

	#endregion

	[SerializeField]
	private Sprite bulletSprite = null;

	//	[FlagsAttribute]
	//	private enum FiringMode
	//	{
	//		NORMAL_ATTACK,
	//		CIRCLE_ATTACK,
	//		SPREAD_SHOT
	//	}

	private bool normal = true;
	private bool spread = false;
	private bool circle = false;

	//    [SerializeField]
	//	private FiringMode firingMode = FiringMode.NORMAL_ATTACK;

	private Fire[] firingTypes = null;

	private const float BULLET_SIZE = .5f;

	private delegate void Fire ();

	private Fire fireFunc;

	private Dictionary<string, List<string>> dictionary = null;

	private AudioSource firingAudio;

	void Start ()
	{
		if (movementSpeed > 8.0f) movementSpeed = 8.0f;
		if (movementSpeed < 4.0f) movementSpeed = 4.0f;

		body2D = GetComponent<Rigidbody2D> ();
		firingAudio = GetComponent<AudioSource> ();

		float vertExtent = Camera.main.orthographicSize;
		float horzExtent = Camera.main.rect.xMax * Camera.main.orthographicSize * Screen.width / Screen.height;

		const float EXTENT_OFFSET = 0.25f;

		xMin = -horzExtent + EXTENT_OFFSET;
		xMax = horzExtent - EXTENT_OFFSET;
		yMin = -vertExtent + EXTENT_OFFSET;
		yMax = vertExtent - EXTENT_OFFSET;

		GameObject upgrades = GameObject.Find ("PersistenceManager");

		//TODO Clean this if possible, or whatever
		if (upgrades != null)
		{
			dictionary = upgrades.GetComponent<Filer> ().GrazeDict;
			foreach (KeyValuePair<string, List<string>> kvp in dictionary)
			{
				if (kvp.Key == "Weapon")
				{
					foreach (string value in kvp.Value)
					{
						if (value == " Scatter")
						{
							spread = true;
						}
						if (value == " Circle")
						{
							circle = true;
						}
					}
				}
			}
		}

		firingTypes = GetFiringTypes ();

		for (int i = 0; i < firingTypes.Length; i++)
		{
			if (firingTypes[i] != null)
				fireFunc += firingTypes[i];
		}
	}

	void OnDestroy ()
	{
		for (int i = 0; i < firingTypes.Length; i++)
		{
			if (firingTypes[i] != null)
				fireFunc -= firingTypes[i];
		}
	}

	private Fire[] GetFiringTypes ()
	{
		Fire[] result = new Fire[10];
		int i = 0;

		if (spread)
		{
			result[i] = SpreadShot;
			i++;
		}
		if (circle)
		{
			result[i] = CircularAttack;
			i++;
		}
		if (normal)
		{
			result[i] = NormalAttack;
			i++;
		}

		return result;
	}

	#region FiringTypes

	private void CircularAttack ()
	{
		GameObject[] bullets = new GameObject[8];
		BulletScript[] bulletScripts = new BulletScript[8];

		for (int i = 0; i < bullets.Length; i++)
		{
			bullets[i] = ObjectPoolerScript_Static.currentPooler.GetPooledObject ();
			bullets[i].SetActive (true);
			bullets[i].transform.position = transform.position;
			bullets[i].GetComponent<SpriteRenderer> ().sprite = bulletSprite;
			bullets[i].tag = "PlayerBullet";
            bullets[i].layer = 0;
            bullets[i].GetComponent<CircleCollider2D> ().offset = new Vector2 (0.0f, 0.55f);
			bullets[i].transform.localScale = Vector3.one * BULLET_SIZE;

			bulletScripts[i] = bullets[i].GetComponent<BulletScript> ();
			bulletScripts[i].Damage = bulletDamage / 3;
			bulletScripts[i].MovementSpeed = bulletSpeed;
			bulletScripts[i].bulletParticle = null;
		}
		bulletScripts[0].MovementDirection = Vector2.up;
		bulletScripts[0].LookAt = 0.0f;
		bulletScripts[1].MovementDirection = Vector2.up + Vector2.right;
		bulletScripts[1].LookAt = 315.0f;
		bulletScripts[2].MovementDirection = Vector2.up + Vector2.left;
		bulletScripts[2].LookAt = 45.0f;
		bulletScripts[3].MovementDirection = Vector2.right;
		bulletScripts[3].LookAt = 270.0f;
		bulletScripts[4].MovementDirection = Vector2.left;
		bulletScripts[4].LookAt = 90.0f;
		bulletScripts[5].MovementDirection = Vector2.down + Vector2.left;
		bulletScripts[5].LookAt = 135.0f;
		bulletScripts[6].MovementDirection = Vector2.down + Vector2.right;
		bulletScripts[6].LookAt = 225.0f;
		bulletScripts[7].MovementDirection = Vector2.down;
		bulletScripts[7].LookAt = 180.0f;

		for (int i = 0; i < bullets.Length; i++)
		{
			bulletScripts[i].OnEnable ();
		}
	}

	private void SpreadShot ()
	{
		GameObject[] bullets = new GameObject[3];
		BulletScript[] bulletScripts = new BulletScript[3];
			
		for (int i = 0; i < bullets.Length; i++)
		{
			bullets[i] = ObjectPoolerScript_Static.currentPooler.GetPooledObject ();
			bullets[i].SetActive (true);
			bullets[i].transform.position = transform.position;
			bullets[i].GetComponent<SpriteRenderer> ().sprite = bulletSprite;
			bullets[i].tag = "PlayerBullet";
            bullets[i].layer = 0;
            bullets[i].GetComponent<CircleCollider2D> ().offset = new Vector2 (0.0f, 0.55f);
			bullets[i].transform.localScale = Vector3.one * BULLET_SIZE;
			
			bulletScripts[i] = bullets[i].GetComponent<BulletScript> ();
			bulletScripts[i].Damage = bulletDamage / 2;
			bulletScripts[i].MovementSpeed = bulletSpeed;
			bulletScripts[i].bulletParticle = null;
		}

		bulletScripts[0].MovementDirection = Vector2.up;
		bulletScripts[0].LookAt = 0.0f;
		bulletScripts[1].MovementDirection = Vector2.up + Vector2.right;
		bulletScripts[1].LookAt = -45.0f;
		bulletScripts[2].MovementDirection = Vector2.up + Vector2.left;
		bulletScripts[2].LookAt = 45.0f;

		for (int i = 0; i < bullets.Length; i++)
		{
			bulletScripts[i].OnEnable ();
		}
	}

	private void NormalAttack ()
	{
		GameObject theBullet = ObjectPoolerScript_Static.currentPooler.GetPooledObject ();
		
		theBullet.transform.position = transform.position;
		theBullet.GetComponent<SpriteRenderer> ().sprite = bulletSprite;
		theBullet.tag = "PlayerBullet";
        theBullet.layer = 0;
        theBullet.GetComponent<CircleCollider2D> ().offset = new Vector2 (0.0f, 0.55f);
		theBullet.transform.localScale = Vector3.one * BULLET_SIZE;
		
		BulletScript theBulletScript = theBullet.GetComponent<BulletScript> ();
		theBulletScript.Damage = bulletDamage;
		theBulletScript.MovementDirection = Vector2.up;
		theBulletScript.MovementSpeed = bulletSpeed;
		theBulletScript.LookAt = 0.0f;
		theBulletScript.bulletParticle = null;
		theBullet.SetActive (true);
	}

	#endregion

	void Update ()
	{
		#region Movement
		Vector2 movementDirection = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));

		body2D.velocity = (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))
			? (movementDirection * REDUCED_MOVEMENT_SPEED)
			: (movementDirection * movementSpeed);

		//TODO Smooth the effect for staying on the screen
		body2D.position = new Vector2 (Mathf.Clamp (body2D.position.x, xMin, xMax), Mathf.Clamp (body2D.position.y, yMin, yMax));

		#endregion

		#region Shooting
		if ((Input.GetKey (KeyCode.Z) || Input.GetKey (KeyCode.J) || Input.GetKey (KeyCode.Space)) && cooldown <= 0 && !PauseMenuScript.IsPaused)
		{
			fireFunc ();
		    if (fireRate > .75f)
		    {
		        fireRate = .75f;
		    }
			cooldown = 1.0f - fireRate;
			firingAudio.Play();
		}

		if (cooldown > 0)
			cooldown -= 1 * Time.deltaTime;
		#endregion
	}

}
