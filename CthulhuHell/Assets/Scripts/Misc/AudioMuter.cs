﻿using UnityEngine;
using System.Collections;

public class AudioMuter : MonoBehaviour
{
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.M) && !AudioListener.pause)
		{
			AudioListener.pause = true;
		}
		else if (Input.GetKeyDown (KeyCode.M) && AudioListener.pause)
		{
			AudioListener.pause = false;
		}
	}
}
