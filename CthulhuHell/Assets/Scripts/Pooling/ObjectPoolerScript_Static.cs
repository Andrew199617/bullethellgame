﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolerScript_Static : MonoBehaviour
{
	public static ObjectPoolerScript_Static currentPooler;

	[SerializeField]
	private GameObject objToPool = null;
	[SerializeField]
	private int numPooledObjects = 0;
	[SerializeField]
	private bool isDynamic = false;

	private List<GameObject> pool;

	[SerializeField]
	private string poolName = null;

	public string PoolName { get { return poolName; } }

	void Awake ()
	{
		currentPooler = this;
	}

	void Start ()
	{
		GameObject poolHolder = null;
		if (poolName != null) poolHolder = GameObject.Find(poolName);

		pool = new List<GameObject> ();
		for (int i = 0; i < numPooledObjects; i++)
		{
			GameObject gObj = (GameObject)Instantiate (objToPool);
			if (poolHolder) gObj.transform.SetParent(poolHolder.transform);
			gObj.SetActive (false);
			pool.Add (gObj);
		}
	}

	public GameObject GetPooledObject()
	{
		for (int i = 0; i < numPooledObjects; i++)
		{
			if (!pool[i].activeInHierarchy)
			{
				return pool[i];
			}
		}

		if (isDynamic)
		{
			GameObject gObj = (GameObject)Instantiate (objToPool);
			gObj.SetActive (false);
			pool.Add (gObj);
			return gObj;
		}

		return null;
	}

}
