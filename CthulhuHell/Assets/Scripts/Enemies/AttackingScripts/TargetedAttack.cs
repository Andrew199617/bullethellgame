﻿using UnityEngine;
using System.Collections;

public class TargetedAttack : EnemyAttack
{
	private GameObject player;
	public float bulletsPerSecond;
	private float dt = 0;
	[SerializeField]
	private ParticleSystem bulletParticle = null;

	void Start ()
	{
		bullets = new GameObject[numBullets];
		player = GameObject.FindGameObjectWithTag ("Player");
    }
    public Vector2 RotateByRadians(Vector2 Center, Vector2 A, float angle)
    {
        //Move calculation to 0,0
        Vector2 v = A - Center;

        //rotate x and y
        float x = v.x * Mathf.Cos(angle) + v.y * Mathf.Sin(angle);
        float y = v.y * Mathf.Cos(angle) - v.x * Mathf.Sin(angle);

        //move back to center
        Vector2 B = new Vector2(x, y) + Center;

        return B;
    }

    void Update()
    {
        Vector2 newPlayerPosition = RotateByRadians(this.transform.position, player.transform.position, 5.49779f);
        Vector3 moveDirection = new Vector3(newPlayerPosition.x, newPlayerPosition.y, 0) - gameObject.transform.position;
		float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
//        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        
        if (dt >= 1 / bulletsPerSecond)
        {
            for (int i = 0; i < numBullets; i++)
            {
				if (bullets[i] == null || (bullets[i] != null && !bullets[i].activeSelf))
                {
					bullets[i] = ObjectPoolerScript_Static.currentPooler.GetPooledObject ();
					bullets[i].transform.position = transform.position;
					bullets[i].GetComponent<SpriteRenderer> ().sprite = bulletSprite;
					bullets[i].GetComponent<CircleCollider2D>().offset = new Vector2(0.0f, 0.0f);
					bullets[i].transform.localScale = Vector3.one * bulletSize;
					bullets[i].tag = "EnemyBullet";
                    bullets[i].layer = 9;

                    BulletScript bulletScript = bullets[i].GetComponent<BulletScript> ();
                    bulletScript.MovementDirection = (player.transform.position - this.transform.position).normalized;
					bulletScript.LookAt = angle - 130.0f;
					bulletScript.MovementSpeed = bulletSpeed;
					bulletScript.bulletParticle = bulletParticle;

					bullets[i].SetActive (true);

					dt = 0;
                    break;
                }
            }
        }
		else
			dt += Time.deltaTime;
	}
}
