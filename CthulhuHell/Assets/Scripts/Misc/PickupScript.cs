﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class PickupScript : MonoBehaviour
{
	public enum PickupType
	{
		LIFE,
		GOLD,
		SCORE
	}

	public PickupType Type { get; set; }

    [SerializeField]
	public float fallSpeed = 2.0f;

	public int Value { get; set; }

	private SpriteRenderer spriteRend;

	[SerializeField]
	private Sprite lifeSprite = null;
	[SerializeField]
	private Sprite goldSprite = null;
	[SerializeField]
	private Sprite scoreSprite = null;

	private PolygonCollider2D polyCollider;

	void Awake()
	{
		spriteRend = gameObject.GetComponent<SpriteRenderer> ();
		polyCollider = this.gameObject.AddComponent<PolygonCollider2D>();
	}

	void Update ()
	{
		transform.Translate (Vector3.down * fallSpeed * Time.deltaTime);
	}

	public void OnEnable()
	{
		gameObject.tag = "Pickup_" + Type.ToString();

        switch (Type)
		{
		case PickupType.LIFE:
			spriteRend.sprite = lifeSprite;
			spriteRend.transform.localScale = Vector3.one * 0.75f;
			Destroy (polyCollider);
            polyCollider = this.gameObject.AddComponent<PolygonCollider2D>();
            polyCollider.isTrigger = true;
            break;
		case PickupType.GOLD:
			spriteRend.sprite = goldSprite;
            spriteRend.transform.localScale = Vector3.one * 2.0f;
			Destroy (polyCollider);
            polyCollider = this.gameObject.AddComponent<PolygonCollider2D>();
            polyCollider.isTrigger = true;
            break;
		case PickupType.SCORE:
			spriteRend.sprite = scoreSprite;
            spriteRend.transform.localScale = Vector3.one * .25f;
			Destroy (polyCollider);
            polyCollider = this.gameObject.AddComponent<PolygonCollider2D>();
            polyCollider.isTrigger = true;
            break;
		default:
			break;
		}
	}

	void OnBecameInvisible()
	{
		gameObject.SetActive (false);
	}

	public void Destroy()
	{
		gameObject.SetActive (false);
	}
}
