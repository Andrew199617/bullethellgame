﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class InfoSerialization
{
	private static BinaryFormatter binaryFormatter = new BinaryFormatter();

	private const string FILE_NAME = "playerSave.ctu";

	public static void Save ()
	{
		FileStream file = File.Create (Application.persistentDataPath + FILE_NAME);
		binaryFormatter.Serialize (file, PlayerInformation.playerInformation.playerData);
		file.Close ();
	}

	public static void Load ()
	{
		if (File.Exists (Application.persistentDataPath + FILE_NAME))
		{
			FileStream file = File.Open (Application.persistentDataPath + FILE_NAME, FileMode.Open);
			PlayerInformation.playerInformation.playerData = (PlayerData)binaryFormatter.Deserialize (file);
			file.Close ();
		}
	}
		
}
