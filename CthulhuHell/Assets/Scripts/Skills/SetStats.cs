﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetStats : MonoBehaviour
{
	private Dictionary<string, List<string>> dictionary;

	void Start ()
	{
		GameObject upgrades = GameObject.Find ("PersistenceManager");
		if (upgrades != null)
		{
			dictionary = upgrades.GetComponent<Filer> ().Dict;
			foreach (KeyValuePair<string, List<string>> kvp in dictionary)
			{
				if (kvp.Key == "fireRate")
				{
					foreach (string value in kvp.Value)
					{
						gameObject.GetComponent<ShipScript> ().fireRate += float.Parse (value);
					}
				}
				if (kvp.Key == "movementSpeed")
				{
					foreach (string value in kvp.Value)
					{
						gameObject.GetComponent<ShipScript> ().movementSpeed += float.Parse (value);
					}
				}
				if (kvp.Key == "bulletDamage")
				{
					foreach (string value in kvp.Value)
					{
						gameObject.GetComponent<ShipScript> ().bulletDamage += float.Parse (value);
					}
				}
			}
		}
	}
}
