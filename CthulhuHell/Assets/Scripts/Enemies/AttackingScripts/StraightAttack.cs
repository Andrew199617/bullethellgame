﻿using UnityEngine;
using System.Collections;

public class StraightAttack : EnemyAttack
{
    [SerializeField]
    private GameObject bullet = null;
    private GameObject bulletHolder;

    private Renderer rend;
    // Use this for initialization
    void Start()
    {
        bullets = new GameObject[numBullets];
        rend = this.GetComponent<Renderer>();
        bulletHolder = GameObject.Find("BulletPool");
    }

    // Update is called once per frame
    void Update()
    {
        if (rend.isVisible)
        {
            if (bullets[0] == null)
            {
                bullets[0] = (GameObject)Instantiate(bullet, this.transform.position, new Quaternion());
                bullets[0].transform.SetParent(bulletHolder.transform);

                BulletScript bulletScript = bullets[0].GetComponent<BulletScript>();
                bulletScript.MovementDirection = Vector3.down;
                bulletScript.MovementSpeed = bulletSpeed;
				bulletScript.bulletParticle = null;
                
            }
            else if (!bullets[0].activeSelf)
            {
                bullets[0].transform.position = this.transform.position;
                bullets[0].SetActive(true);
            }
        }
    }
}
