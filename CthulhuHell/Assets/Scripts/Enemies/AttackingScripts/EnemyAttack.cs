﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
	protected GameObject[] bullets;

	[SerializeField]
	protected Sprite bulletSprite;
	[SerializeField]
	protected float bulletSpeed = 3.0f;
	[SerializeField]
	protected int numBullets = 10;
	[SerializeField]
	protected float bulletSize = 1.0f;
}
