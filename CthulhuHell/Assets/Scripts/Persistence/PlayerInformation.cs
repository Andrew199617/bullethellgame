﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerInformation : MonoBehaviour
{
	public static PlayerInformation playerInformation;
	public PlayerData playerData = new PlayerData ();

	void Awake ()
	{

		if (playerInformation == null)
		{
			DontDestroyOnLoad (transform.gameObject);
            playerData.CurrentRound = 1;
            playerInformation = this;
		}
		else if (playerInformation != this)
		{
			Destroy (gameObject);
		}
        
    }

	void Start ()
	{
		InfoSerialization.Load ();
	}

    public void Load()
    {
        InfoSerialization.Load();
    }

	public void Save()
	{
		InfoSerialization.Save ();
	}

}

[System.Serializable]
public class PlayerData
{
    public void ResetEverything()
    {
        ReInitialize();
        Gold = 50;
        Graze = 25;
        MovementSpeed = 4;
        Damage = 2;
        FireRate = .6f;
        FireRateUpgrades = null;
        MovementSpeedUpgrades = null;
        DamageUpgrades = null;


    }
    public void AddHighScore(int highscore)
    {
        for(int i = 0; i < highScores.Length;i++)
        {
            if(highscore > highScores[i] )
            {
                MoveScoresBackOne(i,highScores[i]);
                highScores[i] = highscore;
                return;
            }
        }
    }

    private void MoveScoresBackOne(int index, int value)
    {
        if(index >= highScores.Length - 1)
        {
            return;
        }
        int newValue = highScores[index + 1];
        highScores[index + 1] = value;
        MoveScoresBackOne(index + 1,newValue); 
    }

    public void ReInitialize()
    {
        if(highScores == null)
        {
            highScores = new int[5];
        }
        Lives = 5;
        Score = 0;
        currentRound = 1;
    }
    public int Score { get; set; }
    public int Lives { get; set; }

    private int currentRound = 1;
    public int CurrentRound { get { return currentRound; } set { currentRound = value; } }
	public int Gold { get; set; }

	public int Graze { get; set; }

	public float MovementSpeed { get; set; }
	public float FireRate { get; set; }
	public int Damage { get; set; }

	public bool[] FireRateUpgrades{ get; set; }
	public bool[] MovementSpeedUpgrades{ get; set; }
	public bool[] DamageUpgrades{ get; set; }

    public int[] highScores;
//	public Dictionary
}