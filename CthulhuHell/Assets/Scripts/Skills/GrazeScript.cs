﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using UnityEngine.UI;

public class GrazeScript : MonoBehaviour
{
    [SerializeField]
    private int price = 10;
    [SerializeField]
    private string effect = "";

	private Toggle toggle;
	private Filer filer;
    private GameObject info;
    private AudioSource audio;


    void Start()
	{
		toggle = gameObject.GetComponent<Toggle> ();
		filer = GameObject.Find ("PersistenceManager").GetComponent<Filer> ();
        info = GameObject.Find("InformationTxt");
        audio = GetComponent<AudioSource>();
    }

    void Update ()
	{
		if (toggle.interactable && toggle.isOn)
	    {
			if (PlayerInformation.playerInformation.playerData.Graze > price)
	        {
				PlayerInformation.playerInformation.playerData.Graze -= price;
				toggle.interactable = false;
                filer.GrazeUpgrades = filer.GrazeUpgrades + effect + "|";
                audio.Play();
            }
        }
    }

    public void EnterMouse()
    {
        info.GetComponent<Text>().text = "Price: " + price + " Effect: " + effect;
    }

    public void ExitMouse()
    {
        info.GetComponent<Text>().text = "";
    }

}
