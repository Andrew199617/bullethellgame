﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour
{

	private static DontDestroy instance;

	void Awake ()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
	}

}
