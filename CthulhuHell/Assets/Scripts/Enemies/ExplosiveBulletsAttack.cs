﻿using UnityEngine;
using System.Collections;

public class ExplosiveBulletsAttack : EnemyAttack {
    [SerializeField]
    private GameObject bullet = null;
    private GameObject bulletHolder;

    private Renderer rend;
    private ExplosionScript explosionScript;
    private float dt;
    // Use this for initialization
    void Start () {
        bullets = new GameObject[numBullets];
        explosionScript = bullet.GetComponent<ExplosionScript>();
        dt = explosionScript.LifeTime() - 1;
        rend = this.GetComponent<Renderer>();
        bulletHolder = GameObject.Find("BulletPool");
        //this.transform.localScale = Vector3.one * 1.25f;
    }
	
	// Update is called once per frame
	void Update () {
        if(rend.isVisible)
        {
            dt += Time.deltaTime;
            if(dt <= explosionScript.LifeTime())
            {
                return;
            }
            if (bullets[0] == null || (bullets[0] != null && !bullets[0].GetComponent<Renderer>().isVisible))
            {
                bullets[0] = (GameObject)Instantiate(bullet, this.transform.position, new Quaternion());
                bullets[0].transform.localScale = Vector3.one * bulletSize;
                bullets[0].tag = "EnemyBullet";
                bullets[0].transform.SetParent(bulletHolder.transform);

                BulletScript bulletScript = bullets[0].GetComponent<BulletScript>();
                bulletScript.MovementDirection = Vector3.down;
                bulletScript.MovementSpeed = bulletSpeed;

                bullets[0].SetActive(true);
                dt = 0;
            }
        }

    }
}
