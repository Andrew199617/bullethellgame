﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetHighScore : MonoBehaviour {
    public int index;
    private Text text;

	// Use this for initialization
	void Start () {
        text = this.GetComponent<Text>();
        text.text = index + " : " + PlayerInformation.playerInformation.playerData.highScores[index - 1];
	}
}
