﻿using UnityEngine;
using System.Collections.Generic;

public class Filer : MonoBehaviour
{
	private string prevUpgrades = "";
	public string Upgrades = "";
	public Dictionary<string, List<string>> Dict = new Dictionary<string, List<string>> ();

	public void CreateDict ()
	{
		prevUpgrades = Upgrades;
		string[] ups = Upgrades.Split ('|');

		foreach (string line in ups)
		{
			if (line != "")
			{
				string[] temp = line.Split (':');
				if (!Dict.ContainsKey (temp[0]))
				{
					Dict.Add (temp[0], new List<string> { temp[1] });
				}
				else
				{
					Dict[temp[0]].Add (temp[1]);
				}
			}
		}
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            Dict.Clear();
            Upgrades = "";
        }
    }

	void OnLevelWasLoaded()
	{
		if (prevUpgrades != Upgrades) CreateDict ();
		if (prevGrazeUpgrades != GrazeUpgrades) CreateGrazeDict ();
	}

	private string prevGrazeUpgrades = "";
    public string GrazeUpgrades = "";
    public Dictionary<string, List<string>> GrazeDict = new Dictionary<string, List<string>>();

    public void CreateGrazeDict()
    {
		prevGrazeUpgrades = GrazeUpgrades;
        string[] ups = GrazeUpgrades.Split('|');

        foreach (string line in ups)
        {
            if (line != "")
            {
                string[] temp = line.Split(':');
                if (!GrazeDict.ContainsKey(temp[0]))
                {
                    GrazeDict.Add(temp[0], new List<string> { temp[1] });
                }
                else
                {
                    GrazeDict[temp[0]].Add(temp[1]);
                }
            }
        }
    }


}


