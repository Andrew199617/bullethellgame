﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyStats : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem deathParticle = null;
    [SerializeField]
    private ParticleSystem bossDeath = null;
    [SerializeField]
    private bool isBoss = false;
    [SerializeField]
	private int health = 10;
	[SerializeField]
	private float movementSpeed = 1;

    [SerializeField]
    private GameObject healthBarPreFab = null;
    private GameObject healthBar;
	[SerializeField]
	private int pointWorth = 0;
	[SerializeField]
	private int goldWorth = 0;
    
	[SerializeField]
	private Vector3 enteringPosition = new Vector3 ();
	[SerializeField]
	private Vector3 exitingPosition = new Vector3 ();

	public float Speed { get { return movementSpeed; } }
	public Vector3 EnteringPosition{ get { return enteringPosition; } }
	public Vector3 ExitingPosition{ get { return exitingPosition; } }

    private float timeHealthShows = 2;
    private float m_dt = 0;
    private GameObject canvas;
    private Slider healthBarSlider;
    private GameHudScript ghs;
	private ObjectPoolerScript_Instance pickupPool;

    void Start()
    {
        canvas = GameObject.Find("Canvas");
		pickupPool = GameObject.Find ("PickupManager").GetComponent<ObjectPoolerScript_Instance> ();
        healthBar = (GameObject)Instantiate(healthBarPreFab, this.transform.position + new Vector3(0, .5f, 0), new Quaternion());
        healthBarSlider = healthBar.GetComponent<Slider>();
        healthBarSlider.maxValue = health;
        healthBar.transform.SetParent(canvas.transform);
        healthBar.SetActive(false);
        ghs = GameObject.Find("GameManager").GetComponent<GameHudScript>();
    }

    void Update()
    {
        if(healthBar != null)
        {
            healthBar.transform.position = this.transform.position + new Vector3(0, .5f, 0);
            m_dt += Time.deltaTime;
        }
        if(m_dt >= timeHealthShows)
        {
            healthBar.SetActive(false);
            m_dt = 0;
        }
    }

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "PlayerBullet")
		{
			health -= Mathf.FloorToInt(other.GetComponent<BulletScript>().Damage);

			if (healthBar) healthBar.SetActive(true);
			if (healthBarSlider) healthBarSlider.value = health;
            m_dt = 0;
			other.gameObject.SetActive(false);
            if(this.movementSpeed == 999)
            {
                this.healthBar.SetActive(false);
                this.gameObject.SetActive(false);
                return;
            }

            if (health <= 0)
            {
                if (isBoss)
                {
                    Instantiate(bossDeath, this.transform.position, new Quaternion());
                }
                else
                {
                    Instantiate(deathParticle, this.transform.position, new Quaternion());
                }

                float dropNum = Random.Range (0.0f, 1.0f);

				// 50% chance at score drop
				if (dropNum > 0.5f)
				{
					GameObject drop = pickupPool.GetPooledObject ();
					drop.transform.position = transform.position;

					PickupScript pickupScript = drop.GetComponent<PickupScript> ();
					pickupScript.Value = pointWorth;
					pickupScript.Type = PickupScript.PickupType.SCORE;
					drop.SetActive (true);
				}
				// 25% chance at gold drop
				else if (dropNum > 0.25f)
				{
					GameObject drop = pickupPool.GetPooledObject ();
					drop.transform.position = transform.position;

					PickupScript pickupScript = drop.GetComponent<PickupScript> ();
					pickupScript.Value = goldWorth;
					pickupScript.Type = PickupScript.PickupType.GOLD;
					drop.SetActive (true);
				}
				// 1% chance at life drop
				else if (dropNum > 0.24f)
				{
					GameObject drop = pickupPool.GetPooledObject ();
					drop.transform.position = transform.position;

					PickupScript pickupScript = drop.GetComponent<PickupScript> ();
					pickupScript.Type = PickupScript.PickupType.LIFE;
					drop.SetActive (true);
				}

                PlayerInformation.playerInformation.playerData.Score += pointWorth / 2;
                ghs.UpdateScore();
                // 20% chance at nothing

                this.healthBar.SetActive(false);
                if(!isBoss)
                {
                    this.gameObject.SetActive(false);
                }
                else
                {
                    Destroy(this.gameObject);
                }
            }
		}
	}

}
