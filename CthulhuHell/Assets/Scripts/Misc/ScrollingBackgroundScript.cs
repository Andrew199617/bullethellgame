﻿using UnityEngine;
using System.Collections;

public class ScrollingBackgroundScript : MonoBehaviour
{
	private const float SCROLL_SPEED = 0.2f;
	private Vector2 savedOffset;

	private Material material;
    public bool Scroll { get; set; }

	void Start ()
	{
        Scroll = true;
		material = GetComponent<Renderer> ().sharedMaterial;
		savedOffset = material.GetTextureOffset ("_MainTex");
	}

	void Update ()
	{
        if(Scroll)
        {
            float y = Mathf.Repeat(Time.time * SCROLL_SPEED, 1);
            Vector2 offset = new Vector2(savedOffset.x, y);
            material.SetTextureOffset("_MainTex", offset);
        }
	}

	void OnDisable ()
	{
		material.SetTextureOffset ("_MainTex", savedOffset);
	}
}
