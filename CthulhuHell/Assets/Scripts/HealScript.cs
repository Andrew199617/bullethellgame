﻿using UnityEngine;
using System.Collections;

public class HealScript : MonoBehaviour {

	public void healOneHeart(int cost)
    {
        if(PlayerInformation.playerInformation.playerData.Lives < 5)
        {
            PlayerInformation.playerInformation.playerData.Gold -= cost;
            PlayerInformation.playerInformation.playerData.Lives++;
            PlayerInformation.playerInformation.Save();
        }
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            PlayerInformation.playerInformation.playerData.ResetEverything();
            PlayerInformation.playerInformation.Save();
        }
    }
}
