﻿using UnityEngine;
using System.Collections;

public class PlayerKiller : MonoBehaviour {
    [SerializeField]
    private GameObject[] hearts = null;

    private PlayerStats playstat;

    [SerializeField]
    public GameObject player;

    private CircleCollider2D circleCollider;

    // Use this for initialization
    void Start () {
        playstat = player.gameObject.GetComponent<PlayerStats>();
        circleCollider = this.GetComponent<CircleCollider2D>();
        updateHearts();
    }

    // Update is called once per frame
    void Update () {
	    
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.tag == "Enemy" || other.tag == "EnemyBullet") && playstat.PlayerInvincibiltyTime <= 0)
        {
            PlayerInformation.playerInformation.playerData.Lives--;
            updateHearts();

            StartCoroutine(playstat.GotHit(circleCollider));


            if (PlayerInformation.playerInformation.playerData.Lives <= 0)
            {
                playstat.GameOver();
            }
            

        }
    }

    public void updateHearts()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            hearts[i].gameObject.SetActive(false);
            if (i < PlayerInformation.playerInformation.playerData.Lives)
            {
                hearts[i].gameObject.SetActive(true);
            }
        }
    }

    

}
