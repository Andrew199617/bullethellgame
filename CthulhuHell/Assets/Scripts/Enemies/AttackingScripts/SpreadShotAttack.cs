﻿using UnityEngine;
using System.Collections;

public class SpreadShotAttack : EnemyAttack
{
    private int timesShot = 0;
    private int bulletsInSpread = 3;
    private bool allBulletsAreNull = true;
    private bool allBulletsArentActive = true;
    private Renderer rend;
    private GameObject bulletHolder;
    private float dt = 0;

    [SerializeField]
    private float bulletsPerSecond = 1;
    [SerializeField]
    private GameObject bullet = null;

	private BulletScript[] bulletScripts;
    // Use this for initialization
    void Start()
    {
        bulletScripts = new BulletScript[numBullets];
        bullets = new GameObject[numBullets];
        rend = this.GetComponent<Renderer>();
        bulletHolder = GameObject.Find("BulletPool");
    }

    // Update is called once per frame
    void Update()
    {
        if (rend.isVisible)
        {
            if (dt >= 1 / bulletsPerSecond)
            {
                allBulletsAreNull = true;
                allBulletsArentActive = false;
                for (int i = bulletsInSpread * timesShot; i < bulletsInSpread * timesShot + bulletsInSpread; i++)
                {
                    if (bullets[i] != null)
                    {
                        allBulletsAreNull = false;
                    }
                    if (bullets[i] != null && !bullets[i].activeSelf)
                    {
                        allBulletsArentActive = true;
                    }
                    else if (bullets[i] == null)
                    {
                        bullets[i] = (GameObject)(Instantiate(bullet, this.transform.position, new Quaternion()));
                        bullets[i].transform.SetParent(bulletHolder.transform);

                        bulletScripts[i] = bullets[i].GetComponent<BulletScript>();
                        bulletScripts[i].MovementSpeed = bulletSpeed;
                    }

                }
                if (allBulletsAreNull)
                {
                    bulletScripts[bulletsInSpread * timesShot].MovementDirection = Vector3.down + Vector3.left;
                    bulletScripts[bulletsInSpread * timesShot + 1].MovementDirection = Vector3.down;
                    bulletScripts[bulletsInSpread * timesShot + 2].MovementDirection = Vector3.down + Vector3.right;
					bulletScripts[bulletsInSpread * timesShot].bulletParticle = null;
					bulletScripts[bulletsInSpread * timesShot + 1].bulletParticle = null;
					bulletScripts[bulletsInSpread * timesShot + 2].bulletParticle = null;
                }
                else if (allBulletsArentActive)
                {
                    for (int i = bulletsInSpread * timesShot; i < bulletsInSpread * timesShot + bulletsInSpread; i++)
                    {
                        bullets[i].transform.position = this.transform.position;
                        bullets[i].SetActive(true);
                    }
                }
                timesShot++;
                if (timesShot >= numBullets / bulletsInSpread)
                {
                    timesShot = 0;
                }
                dt = 0;
            }
            else
            {
                dt += Time.deltaTime;
            }
        }
    }
}
    
