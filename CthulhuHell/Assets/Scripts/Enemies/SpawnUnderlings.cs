﻿using UnityEngine;
using System.Collections;

public class SpawnUnderlings : MonoBehaviour {
    [SerializeField]
    private int maxEnemies = 2;
    [SerializeField]
    private float spawnRate = 2;
    [SerializeField]
    private GameObject underling = null;

    private int enemiesSpawned = 0;
    private GameObject[] enemies;
    private GameObject enemyHolder;
    private float dt = 0;
    // Use this for initialization
    void Start () {
        enemies = new GameObject[maxEnemies];
        enemyHolder = GameObject.Find("Enemies");
        if (enemyHolder == null)
        {
            enemyHolder = GameObject.Find("EnemyPool");
        }
    }
	
	// Update is called once per frame
	void Update () {
	    if(enemiesSpawned < maxEnemies)
        {
            dt += Time.deltaTime;
            if (dt > spawnRate)
            {
                GameObject enemy;
                int movementPattern = UnityEngine.Random.Range(0, (int)EnemyMovement.MovementPatterns.CIRCLE + 1);
                enemy = (GameObject)Instantiate(underling, this.transform.position, new Quaternion());
                enemies[enemiesSpawned] = enemy;

                EnemyMovement enemyMovement = enemy.GetComponent<EnemyMovement>();
                enemyMovement.setMovementPattern(movementPattern);
                enemyMovement.setComeFromRight(false);

                enemy.transform.SetParent(enemyHolder.transform);
                enemiesSpawned++;
                dt = 0;
            }
        }
	}

    void OnDestroy()
    {
        for(int i = 0; i < enemiesSpawned;i++)
        {
            Destroy(enemies[i]);
        }
    }
}
